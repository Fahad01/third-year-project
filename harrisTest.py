import cv2
import numpy as np
import os
import time
import math

path_name = "2.png"

image = cv2.imread(path_name)
grey = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
dst = cv2.cornerHarris(grey, 5, 3, 0.04)

image[dst > 0.1 * dst.max()] = [0, 0, 255]

while True:
    cv2.imshow("image", image)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
cv2.destroyAllWindows()