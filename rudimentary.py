import cv2
import numpy as np
import os
import time
import math

path_name = "2.png"

image = cv2.imread(path_name)

#does not count outer edge
# chessboardCells = []
# for i in range(8):
#         for j in range(8):
#             x1 = ((675/8) * j) + 350
#             y1 = ((675/8) * i) + 25
#             x2 = ((675/8) * (j+1)) +350
#             y2 = ((675/8) * (i+1)) + 25

#             chessboardCells.append([x1,x2, y1, y2])
#             cv2.rectangle(image, (int(x1), int(y1)), (int(x2), int(y2)), (0, 255, 0), 3)
#             cv2.putText(image, str(i)+","+str(j), (int(x1),int(y1)), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2)

#counts outer edge
for i in range(9):
    for j in range(9):
        x1 = 675/8 * i + 350
        y1 = 675/8 * j + 25
        x2 = 675/8 * i+1 +350
        y2 = 675/8 * j+1 + 25
        
        cv2.rectangle(image, (int(x1), int(y1)), (int(x2), int(y2)), (0, 255, 0), 2)
        cv2.putText(image, str(i)+","+str(j), (int(x1),int(y1)), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2)

while True:
    cv2.imshow("image", image)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
cv2.destroyAllWindows()