import cv2
import numpy as np
import os
import time
import math

def get_iou(box1, box2):
    #calculate intersection over union between two bounding boxes

    #ensure boxes overlap
    if box1['x1'] < box1['x2'] and box1['y1'] < box1['y2'] and box2['x1'] < box2['x2'] and box2['y1'] < box2['y2']:  
        
        # find extremities of both bounding boxes
        left = max(box1['x1'], box2['x1'])
        top = max(box1['y1'], box2['y1'])
        right = min(box1['x2'], box2['x2'])
        bottom = min(box1['y2'], box2['y2'])

        if right < left or bottom < top:
            return 0

        #find area of intersection
        intersection = (right - left) * (bottom - top)

        # find area of bounding boxes
        box1_area = (box1['x2'] - box1['x1']) * (box1['y2'] - box1['y1'])
        box2_area = (box2['x2'] - box2['x1']) * (box2['y2'] - box2['y1'])

        # find intersection over union
        iou = intersection / float(box1_area + box2_area - intersection)
        return iou

#print 9x9 dots on image
#each dot should align with a chessboard cell corner
def print_grid():
    for i in range(9):
        for j in range(9):
            x1 = 675/8 * i + 350
            y1 = 675/8 * j + 25
            x2 = 675/8 * i+1 +350
            y2 = 675/8 * j+1 + 25
            
            cv2.rectangle(frame, (int(x1), int(y1)), (int(x2), int(y2)), (0, 255, 0), 2)
            cv2.putText(frame, str(i)+","+str(j), (int(x1),int(y1)), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2)

#initialise chessboard array with vertices
def create_board_array(chessboardCells):
    for i in range(8):
        for j in range(8):
            x1 = ((675/8) * j) + 350
            y1 = ((675/8) * i) + 25
            x2 = ((675/8) * (j+1)) +350
            y2 = ((675/8) * (i+1)) + 25

            chessboardCells.append([x1,x2, y1, y2])

## extract bounding boxes, confidence levels and classes from output of each layer of the network
def extract_output(layer_outputs, boxes, confidences, class_ids):
    for output in layer_outputs:
        #loop over each detection
        for detection in output:
            #extract the class and confidence of current object
            scores = detection[5:]
            class_id = np.argmax(scores)
            confidence = scores[class_id]

            #filter low confidence detections
            if confidence > CONFIDENCE_THRESHOLD:

                #get coordinates of bounding box
                box = detection[:4] * np.array([w, h, w, h])
                (centerX, centerY, width, height) = box.astype("int")

                # calculate edges from centre of bounding box
                x = int(centerX - (width / 2))
                y = int(centerY - (height / 2))

                # append relevant data
                boxes.append([x, y, int(width), int(height)])
                confidences.append(float(confidence))
                class_ids.append(class_id)

##add overlay of bounding boxes, confidence scores and class onto frame
def print_bb(frame, detections):
    if len(detections) > 0:
        #iterate through detections
        for i in detections.flatten():
            # extract the bounding box coordinates
            x, y = boxes[i][0], boxes[i][1]
            w, h = boxes[i][2], boxes[i][3]
            # draw bounding box and labels on detection
            color = [int(c) for c in colours[class_ids[i]]]
            cv2.rectangle(frame, (x, y), (x + w, y + h), color=color, thickness=1)
            text = f"{labels[class_ids[i]]}: {confidences[i]:.2f}"
            # calculate text width & height to draw the transparent boxes as background of the text
            (text_width, text_height) = cv2.getTextSize(text, cv2.FONT_HERSHEY_SIMPLEX, fontScale=1, thickness=1)[0]
            box_coords = ((x, y - 5), (x + text_width + 2, y - 5 - text_height))
            overlay = frame.copy()
            cv2.rectangle(overlay, box_coords[0], box_coords[1], color=color, thickness=cv2.FILLED)
            # add overlay with increased transparency
            frame = cv2.addWeighted(overlay, 0.6, frame, 0.4, 0)
            # add text
            cv2.putText(frame, text, (x, y - 5), cv2.FONT_HERSHEY_SIMPLEX, fontScale=1, color=(0, 0, 0), thickness=1)

def create_board_predictions(chessboardCells, detections):
    #count total number of pieces detected
    count = 0
    #initialise array to represent chessboard
    boardArr = [["  "]*8 for i in range(8)]

    for i in range(len(chessboardCells)):
        
        for j in detections.flatten():
            #get bounding box corner coordinates
            x = boxes[j][0]
            y = boxes[j][1]
            width = boxes[j][2]
            height = boxes[j][3]

            #bounding box corners
            box1 = {'x1' : float(x), 'x2' : float(x + width), 'y1' : float(y), 'y2' : float(y + height)}

            #chessboard tile corners
            box2 = {'x1' : float(chessboardCells[i][0]), 'x2' : float(chessboardCells[i][1]), 'y1' : float(chessboardCells[i][2]), 'y2' : float(chessboardCells[i][3])}

            #validate iou above threshold
            if get_iou(box2, box1)>0.2:
                count+=1
                boardRow = math.floor(i/8)
                boardCol = i%8
                boardArr[boardCol][boardRow] = labels[class_ids[j]]
                break

    print("count: "+ str(count))

    #print board array
    for i in range(len(boardArr)):
        boardArr[i].reverse()
        print(boardArr[i])



CONFIDENCE_THRESHOLD = 0.4
SCORE_THRESHOLD = 0.4
IOU_THRESHOLD = 0.2

# yolo neural network config
config = "weights/detector.cfg"
# weights
weights = "weights/final.weights"

# class labels
labels = open("weights/obj.names").read().strip().split("\n")

#select random colour for each class
colours = np.random.randint(0, 255, size=(len(labels), 3), dtype="uint8")


net = cv2.dnn.readNetFromDarknet(config, weights)

cap = cv2.VideoCapture(0)

while True:

    chessboardCells = []
    create_board_array(chessboardCells)

    ret, frame = cap.read()
    h, w = frame.shape[:2]
    # convert image to blob
    blob = cv2.dnn.blobFromImage(frame, 1/255.0, (416, 416), swapRB=True, crop=False)

    # neural network input
    net.setInput(blob)

    #retrieve all layers
    layer_names = net.getLayerNames()
    #filter only output layers
    layer_names = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]

    # begin inference of input/ pass input through neural network
    layer_outputs = net.forward(layer_names)

    #initialise variables
    boxes = [] 
    confidences = [] 
    class_ids = []

    #get results
    extract_output(layer_outputs, boxes, confidences, class_ids)

    #apply non maximum supression
    detections = cv2.dnn.NMSBoxes(boxes, confidences, SCORE_THRESHOLD, IOU_THRESHOLD)

    #print bound boxes on frame
    print_bb(frame, detections)

    #print alignment grid
    print_grid()

    #evaluate board if enter clicked
    if cv2.waitKey(1) & 0xFF == ord('\r'):
        create_board_predictions(chessboardCells, detections)

    cv2.imshow('frame', frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
