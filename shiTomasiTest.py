import cv2
import numpy as np
import os
import time
import math

path_name = "2.png"

image = cv2.imread(path_name)
grey = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

corners = cv2.goodFeaturesToTrack(grey, 81, 0.01, 40)
print(corners)
for i in corners:
    #print(i)
    #print("------")
    #print(i.ravel())
    #exit()
    x, y = i.ravel()
    cv2.circle(image, (x, y), 3, (0, 0, 255), -1)

while True:
    cv2.imshow("image", image)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
cv2.destroyAllWindows()