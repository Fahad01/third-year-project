# Third Year Project

This repository contains code for the computer vision chess application

**Note** It is unlikely you will be able to properly run the program. This is because the weights were trained using my specific chess pieces. Therefore, it is unlikely any other chess pieces would be detected during inference.

**Note** The weight file are quite large

Instructions to run program:

- Ensure OpenCV installed
- Ensure Numpy installed
- Ensure camera is connected
- type "python3 pieceRecognition.py" to run python script
